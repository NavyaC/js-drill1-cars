const inventory = require('./inventory.cjs');

function problem2(inventory) {
    if (arguments.length !== 2 || inventory.length === 0 || !Array.isArray(inventory) || (checkId < 1 && checkId > 50) || typeof (checkId) != 'number') {
        return [];
    }
    return inventory[inventory.length-1];
    //`Last car is a ${inventory[inventory.length-1].car_make} ${inventory[inventory.length-1].car_model}`;
};

module.exports = problem2;
