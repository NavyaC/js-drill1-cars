const inventory = require('./inventory.cjs');

function problem3(inventory) {
    if (arguments.length !== 2 || inventory.length === 0 || !Array.isArray(inventory) || (checkId < 1 && checkId > 50) || typeof (checkId) != 'number') {
        return [];
    }
    let arrCars = []
    for (let index=0; index < inventory.length; index++) {
        arrCars.push(inventory[index].car_model);
    }
    return arrCars.sort();
};

module.exports = problem3;
